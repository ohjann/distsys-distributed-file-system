import socket
import time
import sys
import os
from optparse import OptionParser


# Setup program flags and descriptions
parser = OptionParser()

parser.add_option("-p","--port", action="store", type="int", dest="pnumber", metavar="PORT NUMBER", help="set custom port number")
parser.add_option("-f","--file", action="store", type="string", dest="filename", metavar="FILENAME", help="use custom file")
parser.add_option("-k","--kill", action="store_true", dest="kill", help="send kill message to server")
(options, args) = parser.parse_args()

if(not options.pnumber and  not options.filename):
    parser.error("incorrect args\nType -h or --help for help.")

if(options.pnumber):
    port = options.pnumber
else:
    port = 8000

if(options.filename):
    path = options.filename
else:
    path = "/echo.php"

def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = 'localhost'
    cache = dict() # cache is simple dictionary with { filename : time-added }
    filelist = [ f for f in os.listdir("./cache") ] # clear cache dir
    for f in filelist:
        os.remove('cache/'+f)
    pwd = "."
    try:
        s.connect((host,port))

        stayAlive = True
        username = ''
        print("Type 'help' for options")
        while(stayAlive):

            #menu = input("What do you want to do?\n\t0. List Files \n\t1. Read a file\n\t2. Write a file\n\t3. Disconnect\n>")
            menu = input("\n> ")
            if(menu):
                if 'ls' in menu:
                    message = path+"?message='LIST "+pwd+"\n'"
                    s.sendall(message.encode('utf-8'))
                    response = (s.recv(1024)).decode()
                    response = response.replace("('","")
                    response = response.replace("',)","")
                    print(response)

                elif 'cd' in menu:
                    command = menu.split()
                    if ".." in command[1]:
                        if pwd == ".":
                            continue
                        pwd = pwd.rsplit("/",1)[0]
                        continue

                    message = path+"?message='LIST "+pwd+"\n'"
                    s.sendall(message.encode('utf-8'))
                    response = (s.recv(1024)).decode()
                    if pwd+"/"+command[1] in response:
                        pwd = pwd+"/"+command[1]

                    else:
                        print("not a directory")

                elif "pwd" in menu:
                    print("/"+pwd)

                elif "cat" in menu:
                    command = menu.split()

                    filename = command[1]
                    if "/" in command[1]:
                        filename = command[1].rsplit('/',1)[1]
                    
                    askserver = True
                    if os.path.exists('cache/'+filename):
                        if filename in cache:
                            print(time.time() - cache[filename])
                            if (time.time() - cache[filename]) > 60: # check cache TTL
                                print("cache timeout")
                                del cache[filename]
                                os.remove('cache/'+filename)
                            else:
                                askserver = False
                                ffile = open('cache/'+filename, 'r')
                                contents = ffile.read()
                                ffile.close()
                        else:
                            askserver = False
                            cache[filename] = time.time()
                            ffile = open('cache/'+filename, 'r')
                            contents = ffile.read()
                            ffile.close()

                    if askserver:
                        message = path+"?message='READ "+pwd+"/"+command[1]+"\n'"
                        s.sendall(message.encode('utf-8'))
                        contents = b''
                        response = s.recv(1024)

                        # get response size
                        size = 0
                        for token in response.decode().split():
                            if token.isdigit():
                                size = int(token)
                                break
                        contents = contents + response
                        contents = '\n'.join(contents.decode().split('\n')[1:])
                        while(True):
                            if(sys.getsizeof(contents) >= size):
                                break
                            if(size>0):
                                response = s.recv(1024)
                            if(response):
                                contents = contents + response.decode()
                            else:
                                break
                        ffile = open('cache/'+filename, 'w+')
                        cache[filename] = time.time()
                        ffile.write(contents)
                        ffile.close()
                    #print(contents)

                elif "echo" in menu:
                    command = menu.split(">")
                    start = s.index( first ) + len( first )
                    end = s.index( last, start )
                    modification = menu[start:end]
                    size = "%d\n" % (sys.getsizeof(contents))
                    message = path+"?message='WRITE "+pwd+"/"+command[1]+"\n"+size+modification+"'"
                    s.sendall(message.encode('utf-8'))
                 
                elif 'exit' in menu:
                    message = path+"?message='DISCONNECT\n'"
                    s.sendall(message.encode('utf-8'))
                    s.close() #close connection once a response has be given
                    stayAlive = False

                elif 'help' in menu:
                    print("'ls' : list files and directories\n'cat [PATH TO FILE]' : view file contents\n'echo '[SOME STRING]' > [PATH TO FILE]' : replace file contents with [SOME STRING]\n'exit' : disconnect and exit\n'help' : bring up this help")



    except Exception as e:
        print("soz no", e)
main()
