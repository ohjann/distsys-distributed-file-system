import sqlite3
import queue
import os
import sys
import socket
import threading
from collections import Counter
from optparse import OptionParser

# Setup program flags and descriptions
parser = OptionParser()
parser.add_option("-d", "--debug", action="store_true", dest="debug", help="put server in debug mode where it is intentionally slowed down")
parser.add_option("-p","--port", action="store", type="int", dest="pnumber", metavar="PORT NUMBER", help="set custom port number, defaults to 8080")
parser.add_option("-i","--IP", action="store", type="string", dest="hnumber", metavar="HOST IP", help="set custom host IP, defaults to empty string")
(options, args) = parser.parse_args()

if(not options.pnumber and (len(args) >0)):
    parser.error("incorrect args\nType -h or --help for help.")

if(options.hnumber):
    host = options.hnumber
else:
    host = 'localhost'
if(options.pnumber):
    port = options.pnumber
else:
    port = 8000
serverstatus = ":)"
clientqueue = queue.Queue()
serverqueue = queue.Queue()
serverreq = queue.Queue()
serverres = queue.Queue()

db = 'dirstructure.db'
conn = sqlite3.connect(db)

c = conn.cursor()

# create a fresh db if one already exists
c.execute('''DROP TABLE IF EXISTS dirlist''')
c.execute('''CREATE TABLE dirlist (id INTEGER PRIMARY KEY , serverip text, directory text, name text)''')


###
###  SERVERS
###
class ThreadServer (threading.Thread):
    """Handles connected clients concurrently"""
    def __init__(self, serverqueue):
        threading.Thread.__init__(self)
        self.serverqueue = serverqueue

    def populateDB(self,addr,directory,file):
        conn = sqlite3.connect(db)
        csr = conn.cursor()
        csr.execute('''INSERT INTO dirlist VALUES (NULL,"'''+addr+'''","'''+directory+'''","'''+file+'''")''')
        conn.commit()
        return
    
    def only_uniques(self, seq): #stolen from stackoverflow
            return [k for k,n in Counter(seq).items() if n == 1]

    def getMissing(self):
        conn = sqlite3.connect(db)
        csr = conn.cursor()
        csr.execute('''select * from dirlist''')
        dirlist = csr.fetchall()
        conn.close()
        #servdir = dict()
        servdir = []
        for dirr in dirlist:
            servdir.append((dirr[1],dirr[2]+'/'+dirr[3]))
            #servdir.setdefault(dirr[1],[])
            #servdir[dirr[1]].append(dirr[2]+'/'+dirr[3])
        unique = []
        for path in servdir:
            unique.append(path[1])
        # get any items that don't occur more than once i.e. files that need to be replicated
        unique = self.only_uniques(unique)
        if(unique == []):
            # nothing missing 
            return

        ##  WARNING FOLLOWING CODE IN FUNCTION HAS NOT BEEN TESTED
        for ffile in unique:
            server = [i for i, v in enumerate(servdir) if v[0] == ffile]

        ## TODO: Send read request to "server" and send write request to other servers with contents


    def run(self):
        global serverstatus
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        serverdetails = self.serverqueue.get()
        shost,sport = serverdetails.split(":")

        print("Connection:",serverdetails)
        print("Host: %s Port: %s" % (shost,sport))

        try:
            s.connect((shost,int(sport)))
        except:
            print("can't connect to server %s:%s" % (shost, sport))
            self.serverqueue.task_done()
            return

        # Populate DB
        s.sendall(("LIST\n").encode())
        servdata = s.recv(1024).decode()
        files = servdata.split(",")
        for file in files:
            pwd = file.rsplit("/",1)
            self.populateDB(serverdetails,pwd[0].strip(),pwd[1].strip())

        # check for missing files
        self.getMissing()

        while (serverstatus == ":)"):
            request = serverreq.get()
            if not all(x in request[1] for x in [shost, sport]): 
                # check if the request is meant for this thread 
                # otherwise put it back in the queue
                serverreq.put(request)
                continue
            
            print(request[0].split('\n',1)[0])
            if "READ" in request[0].split('\n', 1)[0]:
                print("Reading from %s:%s" % (shost,sport))
                s.sendall(("READ "+request[0].split('\n', 1)[1]).encode())
                contents = b''
                response = s.recv(1024)

                # get response size
                size = 0
                for token in response.decode().split():
                    if token.isdigit():
                        size = int(token)
                        break
                contents = contents + response
                while(True):
                    if(sys.getsizeof(contents) >= size):
                        break
                    if(size>0):
                        try:
                            response = s.recv(1024)
                        except:
                            break
                    if(response):
                        contents = contents + response
                    else:
                        break
                serverres.put(contents)
                serverreq.task_done()

            elif "WRITE" in request[0].split('\n', 1)[0]:
                filetowrite = request[0].split("WRITE",1)[1].split("\n",1)[0].strip()
                print("Writing to %s:%s/%s" % (shost,sport,filetowrite))
                s.sendall(request[0].encode())
                response = s.recv(1024)
                serverres.put(response)
                serverreq.task_done()


###
###  CLIENTS
###

class ThreadClients (threading.Thread):
    """Handles connected clients concurrently"""
    def __init__(self, clientqueue):
        threading.Thread.__init__(self)
        self.clientqueue = clientqueue

    def run(self):
        global serverstatus
        connection = self.clientqueue.get()
        print("Client thread running")

        while (serverstatus == ":)"):
            try:
                data = connection[0].recv(1024).decode()
            except: break
            if "HELO" in data:
                print ("*Sending message to ", connection[1][0])
                passedText = data.split("HELO",1)[1].split("\n",1)[0].strip()
                textToSend = ("HELO "+passedText+"\nIP:"+host+"\nPort:"+str(port)+"\nStudentID:11424478").encode()
                connection[0].sendall(textToSend)

            elif "KILL_SERVICE" in data:
                print ("*Kill request recieved from ", connection[1][0])
                serverstatus = ":("
                connection[0].sendall(("Server terminating\n").encode())
                break

            elif "DISCONNECT" in data:
                print("*Client %s disconnecting" % (connection[1][0]))
                connection[0].close()
                self.clientqueue.task_done()

            elif "LIST" in data:
                pwd = data.split("LIST",1)[1].split("\n",1)[0].strip()
                conn = sqlite3.connect(db)
                csr = conn.cursor()
                csr.execute('select name from dirlist where directory like "'+pwd+'"')  
                ls = [record for record in csr.fetchall()]
                csr.execute('select distinct directory from dirlist where directory glob"'+pwd+'/*"')  
                ls = ls + [record for record in csr.fetchall()]
                ls = str(ls).strip('[]')
                connection[0].sendall((ls).encode())

            elif "READ" in data:
                path = data.split("READ",1)[1].split("\n",1)[0].strip()
                directory, filename = path.rsplit("/",1)
                conn = sqlite3.connect(db)
                csr = conn.cursor()
                csr.execute('select serverip from dirlist where directory glob "*'+directory+'" and name like "'+filename+'" limit 1')  
                ls = [record for record in csr.fetchall()]
                ls = str(ls).strip("[]")
                serverreq.put(("READ\n"+path,ls))
                content = serverres.get()
                connection[0].sendall(content)
            
            elif "WRITE" in data:
                path = data.split("WRITE",1)[1].split("\n",1)[0].strip()
                directory, filename = path.rsplit("/",1)

                contents = ""
                size = 0
                response = data.split("WRITE",1)[1].split("\n",1)[1].strip()
                for token in response.split():
                    if token.isdigit():
                        size = int(token)
                        break
                contents = contents + response
                while(True):
                    if(sys.getsizeof(contents) >= size):
                        break
                    if(size>0):
                            response = connection[0].recv(1024)
                    if(response):
                        contents = contents + response.decode()
                    else:
                        break

                csr.execute('select serverip from dirlist where directory glob "*'+directory+'" and name like "'+filename+'" limit 1')  
                ls = [record for record in csr.fetchall()]
                ls = str(ls).strip("[]")
                serverreq.put(("WRITE "+path+"\n"+contents, ls))
                serres = serverres.get()
                connection[0].sendall(serres)
 
            else:
                print ("*Malformed request from", connection[1][0])
                connection[0].sendall(("ERROR: Malformed request\n").encode())

            if (options.debug): time.sleep(2)
            if (data == ''): break

def startCThread(queue):
    thread = ThreadClients(queue)
    thread.setDaemon(True)
    thread.start()

def startSThread(queue):
    thread = ThreadServer(queue)
    thread.setDaemon(True)
    thread.start()

def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    s.bind((host, port))
    s.listen(10)

    for i in range(5):
        startCThread(clientqueue)

    for i in range(5):
        startSThread(serverqueue)

    # directory server reads in associated file servers from file
    # servers are identified by IP:PORT
    servfile = open("serverlist.txt","r")
    servlist = servfile.read()
    servlist = servlist.splitlines()
    for server in servlist:
        serverqueue.put(server)

    while True:
        print("\t\t"+serverstatus)
        if(serverstatus == ":("): break
        conn, addr = s.accept()
        # don't allow any more than 6 servers to connect at once
        print('Connected by ', addr, clientqueue.qsize())
        clientqueue.put((conn,addr)) # tuple in form of (socket connection, (server address))
        clientqueue.join()
        startCThread(clientqueue)

    print("Exiting");
    s.close()

main()
