#CS4032 Distributed File Server
###server.py
Contains code to be run on each of the file servers

###dirserv.py
Contains code to be run on server to be used as directory server which the client connects to, server.txt contains a list of all servers which the dirserv should attempt to connect to. Also contains unfinished code for replication amongst servers.

###iclient.py
Contains an interactive terminal-simulator for transparent directory access, should be seperated into client and client-proxy in future,  also contains a simple browser-esque cache where the TTL is 60 seconds.
