import socket
import time
from optparse import OptionParser
import sys


# Setup program flags and descriptions
parser = OptionParser()

parser.add_option("-p","--port", action="store", type="int", dest="pnumber", metavar="PORT NUMBER", help="set custom port number")
parser.add_option("-f","--file", action="store", type="string", dest="filename", metavar="FILENAME", help="use custom file")
parser.add_option("-k","--kill", action="store_true", dest="kill", help="send kill message to server")
(options, args) = parser.parse_args()

if(not options.pnumber and  not options.filename):
    parser.error("incorrect args\nType -h or --help for help.")

if(options.pnumber):
    port = options.pnumber
else:
    port = 8000

if(options.filename):
    path = options.filename
else:
    path = "/echo.php"


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = 'localhost'

pwd = '.'

try:
    s.connect((host,port))

    message = path+"?message='HELO basic\n'"
    s.sendall(message.encode('utf-8'))
    response = s.recv(1024)
    print(response)

    message = path+"?message='LIST "+pwd+"\n'"
    s.sendall(message.encode('utf-8'))
    response = s.recv(1024)
    print(response)
    print("------------------------------------------------")
    print("------------------------------------------------")

    message = path+"?message='READ textfiles/harrypotter2.txt\n'"
    s.sendall(message.encode('utf-8'))
    contents = b''
    response = s.recv(1024)

    # get response size
    size = 0
    for token in response.decode().split():
        if token.isdigit():
            size = int(token)
            print(size)
            break
    contents = contents + response
    contents = '\n'.join(contents.decode().split('\n')[1:])
    while(True):
        if(sys.getsizeof(contents) >= size):
            break
        if(size>0):
            response = s.recv(1024)
        if(response):
            contents = contents + response.decode()
        else:
            break

    #print(contents)
    contents = contents + str(time.clock())

    size = "%d\n" % (sys.getsizeof(contents))
    message = path+"?message='WRITE textfiles/harrypotter2.txt\n"+size+contents+"'"
    s.sendall(message.encode('utf-8'))

    response = s.recv(1024)
    print(size)
    print(response.decode())
    message = path+"?message='DISCONNECT\n'"
    s.sendall(message.encode('utf-8'))

    s.close() #close connection once a response has be given


except Exception as e:
    print("soz no", e)
